#include "Station.h"
//#include "pch.h"


Station::Station(const std::string& name):_name(name) {}

std::string Station::GetName() const {
	return _name;
}

std::string Station::GetType() const {
	return "Station";
}

void Station::SetName(const std::string& name) {
	_name = name;
}

Vector<std::string> Station::GetLines() const {
	 return {};

}


TransferStation::TransferStation(const std::string& name, const Vector<std::string>& lines):Station(name), _lines(lines) {}

Vector<std::string> TransferStation::GetLines() const {
	return _lines;
}

std::string TransferStation::GetType() const{
	return "TransferStation";
}

void TransferStation::AddLine(const std::string& name) {
	_lines.PushBack(name);
}


TransferVertex::TransferVertex(const std::string& name, const Vector<StationInfo>& stations):Station(name), _stations(stations) {}

Vector<StationInfo> TransferVertex::GetStationInfo() const {
	return _stations;
}

std::string TransferVertex::GetType() const {
	return "TransferVertex";
}

void TransferVertex::AddTransfer(const std::string& name, const std::string& line) {
	StationInfo new_station{name,line};
	_stations.PushBack(new_station);
}

void TransferVertex::PrintStationsLines() {
	for (int i = 0; i < _stations.Size(); i++) {
		std::cout << _stations[i].station << "  " << _stations[i].line;
	}
}



StationDescriber::StationDescriber(Station* station, const Vector<std::string> line_name):_station(station),_line_name(line_name){}

StationDescriber::~StationDescriber() {
	delete _station;
}

std::string StationDescriber::GetStationName() const {
	return _station->GetName();
}

void StationDescriber::ShowStation() const {
	std::cout << _station->GetName() << "  " << _station->GetType() << "  " << _line_name[0] << "\n";
}

void StationDescriber ::SetStationName(const std::string& name) {
	_station->SetName(name);
}

std::string StationDescriber::GetStationType() const {
	return _station->GetType();
}

void StationDescriber::ChangeStationType(const std::string& type) {
	if (_station->GetType() == type) {
		return;
	}
	
	if (type == "Station") {
		std::string name = _station->GetName();
		delete _station;
        _station = new Station(name);
		return;
	}

	if (type == "TransferStation") {
		std::string name = _station->GetName();
		delete _station;
		_station = new TransferStation(name,{});
		return;
	}

	if (type == "TransferVertex") {
		std::string name = _station->GetName();
		delete _station;
		_station = new TransferVertex(name,{});
		return;
	}
}



LineDescriber::LineDescriber(const Vector<StationDescriber*> stations):_stations(stations){}

LineDescriber::LineDescriber(std::string line_name):_line_name(line_name){}

void LineDescriber::AddStationDescriber(Station* station, const std::string& line_name) {
	_stations.PushBack ( new StationDescriber ( station, Vector<std::string>( { line_name } ) ) ); // ��������� �������� � �� ��� ��� ����������� ��� 

}

StationDescriber* LineDescriber::FindStation(const std::string& station_name) {
	for (size_t i = 0; i < _stations.Size(); i++) {
		if (station_name == _stations[i]->GetStationName()) {
			return _stations[i];
		}
	}
	return NULL;
}

void LineDescriber::ShowTable() {
	if (_stations.Size() == 0) {
		std::cout << "This line has no stations!\n";
	}
	for (size_t i = 0; i < _stations.Size(); i++) {
		_stations[i]->ShowStation();
	}
}

std::string LineDescriber::GetName() const{
	return _line_name;
}

void LineDescriber::DeleteStation(const std::string& station_name) { // !?
	for (int i = 0; i < _stations.Size(); i++) {
		if (_stations[i]->GetStationName() == station_name) {
			for (int j = i; j < _stations.Size() - i - 1; i++) {
				_stations[j] = _stations[j + 1];
			}
		}
	}
	_stations.PopBack();
}

LineDescriber::~LineDescriber() {
	for (int i = 0; i < _stations.Size(); i++) {
		delete _stations[i];
	}
}


void Metro::AddLine(const std::string& line_name) {
	for (size_t i = 0; i < _lines.Size(); i++) {
		if (line_name == _lines[i].GetName()) {
			return;
		}
	}
	_lines.PushBack(LineDescriber(line_name));
}

void Metro::DeleteLine(const std::string& line_name) {
	for (size_t i = 0; i < _lines.Size(); i++) {
		if (line_name == _lines[i].GetName()) {
			std::swap(_lines[i], _lines.Back());
			_lines.PopBack();
			return;
		}
	}
}

void Metro::DeleteStation(const std::string& station_name) {
	for (int i = 0; i < _lines.Size(); i++) {
		if (_lines[i].FindStation(station_name) != NULL) {
			_lines[i].DeleteStation(station_name);
			return;
		}
	}
}

void Metro::ChangeStationName(const std::string& line_name, const std::string& station_name, const std::string& new_station_name) {
	for (size_t i = 0; i < _lines.Size(); i++) {
		if (line_name == _lines[i].GetName()) {
			_lines[i].FindStation(station_name)->SetStationName(new_station_name);
		}
	}
}

void Metro::ChangeStationType(const std::string& line_name, const std::string& station_name, const std::string& type_name) {
	for (size_t i = 0; i < _lines.Size(); i++) {
		if (line_name == _lines[i].GetName()) {
			_lines[i].FindStation(station_name)->ChangeStationType(type_name);
		}
	}
}

StationDescriber* Metro::FindStation(const std::string& line_name, const std::string& station_name) {

	for (size_t i = 0; i < _lines.Size(); i++) {
		if (line_name == _lines[i].GetName()) {
			return _lines[i].FindStation(station_name);
		}
	}
	return NULL;
}

void Metro::ShowMetro() {
	if (_lines.Size() == 0) {
		std::cout << "no lines exist\n";
		return;
	}
	for (size_t i = 0; i < _lines.Size(); i++) {
		std::cout <<i<<". "<< _lines[i].GetName() << "\n";
	}
}

void Metro::ShowLine(const std::string& line_name) {
	for (size_t i = 0; i < _lines.Size(); i++) {
		if (line_name == _lines[i].GetName()) {
			_lines[i].ShowTable();
			return;
		}
	}
	std::cout << "No such line!\n";
}

void Metro::AddLine(const std::string& line_name, const std::string& station_name, const std::string& type_name) {
	for (size_t i = 0; i < _lines.Size(); i++) {
		if (line_name == _lines[i].GetName()) {
			_lines[i].AddStationDescriber(new Station(station_name), { line_name });
		}
	}
}
void Metro::AddStation(const std::string& line_name,Station* station) {
	for (size_t i = 0; i < _lines.Size(); i++) {
		if (line_name == _lines[i].GetName()) {
			_lines[i].AddStationDescriber(station, line_name);
		}
	}
}

StationDescriber* Metro::FindStation(const std::string& station_name) {
	for (size_t i = 0; i < _lines.Size(); i++) {
		if (_lines[i].FindStation(station_name)!= NULL) {
			return _lines[i].FindStation(station_name);
		}
	}
	return NULL;
}

LineDescriber* Metro::FindLine(const std::string& line_name) {
	for (int i = 0; i < _lines.Size(); i++) {
		if (_lines[i].GetName() == line_name) {
			return &_lines[i];
		}
	}
	return NULL;
}

Metro::~Metro() = default;