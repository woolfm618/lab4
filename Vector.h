#pragma once
#include <exception>
#include <iostream>

template <typename T>
class Vector {

private:
    size_t _size;
    size_t _capacity;
    T* _data;

private:
    T* Realoc(size_t new_capacity);

public:
    Vector();
    Vector(const std::initializer_list<T>& list);
    explicit Vector(size_t size);
    Vector(size_t size, const T& value);
    Vector(const Vector& other);
    Vector(Vector&& other);                            //������ ������ 
    ~Vector();
    Vector& operator=(const Vector& other);
    Vector& operator=(Vector&& other);
    size_t Size() const;
    size_t Capacity() const;
    bool Empty() const;
    T& operator[](size_t i);
    const T& operator[](size_t i) const;
    T& Back();
    const T& Back() const;
    T& Front();
    const T& Front() const;
    T* Data();
    const T* Data() const;
    void Reserve(size_t new_capacity);
    void Clear();
    void PushBack(const T& elem);
    void PushBack(T&& elem);
    void PopBack();
};

template <typename T>
Vector<T>::Vector() : _size(0), _capacity(0), _data(nullptr) {
}

template <typename T>
Vector<T>::Vector(size_t size)
    : _size(size)
    , _capacity(_size)
    , _data(_size > 0 ? reinterpret_cast<T*>(new uint8_t[sizeof(T) * _capacity]) : nullptr) {
    size_t i = 0;
    try {
        for (; i < _size; ++i) {
            new (_data + i) T();  // ������ � ����� ������� ��� ������� 
        }
    }
    catch (...) {
        for (size_t j = 0; j < i; ++j) {
            _data[j].~T();
        }
        delete[] reinterpret_cast<uint8_t*>(_data);
        throw;
    }
}

template <typename T>
Vector<T>::Vector(size_t size, const T& value)
    : _size(size)
    , _capacity(_size)
    , _data(_size > 0 ? reinterpret_cast<T*>(new uint8_t[sizeof(T) * _capacity]) : nullptr) {
    size_t i = 0;
    try {
        for (; i < _size; ++i) {
            new (_data + i) T(value);
        }
    }
    catch (...) {
        for (size_t j = 0; j < i; ++j) {
            _data[j].~T();
        }
        delete[] reinterpret_cast<uint8_t*>(_data);
        throw;
    }
}

template <typename T>
Vector<T>::Vector(const Vector& other)
    : _size(other._size)
    , _capacity(_size)
    , _data(_size > 0 ? reinterpret_cast<T*>(new uint8_t[sizeof(T) * _capacity]) : nullptr) {
    size_t i = 0;
    try {
        for (; i < _size; ++i) {
            new (_data + i) T(other._data[i]);
        }
    }
    catch (...) {
        for (size_t j = 0; j < i; ++j) {
            _data[j].~T();
        }
        delete[] reinterpret_cast<uint8_t*>(_data);
        throw;
    }
}

template <typename T>
Vector<T>::Vector(Vector&& other) : _size(other._size), _capacity(other._capacity), _data(other._data) {
    other._data = nullptr;
    other._size = 0;
    other._capacity = 0;
}

template <typename T>
Vector<T>::~Vector() {
    for (size_t i = 0; i < _size; ++i) {
        _data[i].~T();
    }
    delete[] reinterpret_cast<uint8_t*>(_data);
}

template <typename T>
Vector<T>& Vector<T>::operator=(const Vector& other) {
    if (this == &other) { // ���� ���� � ��� �� ������ 
        return *this;
    }
    auto new_data = other._size > 0 ? reinterpret_cast<T*>(new uint8_t[sizeof(T) * other._size]) : nullptr;
    size_t i = 0;
    try {
        for (; i < other._size; ++i) {
            new (new_data + i) T(other._data[i]);
        }
    }
    catch (...) {
        for (size_t j = 0; j < i; ++j) {
            new_data[j].~T();
        }
        delete[] reinterpret_cast<uint8_t*>(new_data);
        throw;
    }
    for (size_t j = 0; j < _size; ++j) {
        _data[j].~T();
    }
    delete[] reinterpret_cast<uint8_t*>(_data);
    _data = new_data;
    _size = other._size;
    _capacity = other._size;
    return *this;
}

template <typename T>
Vector<T>& Vector<T>::operator=(Vector&& other) {
    if (this == &other) {
        return *this;
    }
    for (size_t i = 0; i < _size; ++i) {
        _data[i].~T();
    }
    delete[] reinterpret_cast<uint8_t*>(_data);
    _data = other._data;
    _size = other._size;
    _capacity = other._capacity;
    other._data = nullptr;
    other._size = 0;
    other._capacity = 0;
    return *this;
}

template <typename T>
size_t Vector<T>::Size() const {
    return _size;
}

template <typename T>
size_t Vector<T>::Capacity() const {
    return _capacity;
}

template <typename T>
bool Vector<T>::Empty() const {
    return _size == 0;
}

template <typename T>
T& Vector<T>::operator[](size_t i) {
    return _data[i];
}

template <typename T>
const T& Vector<T>::operator[](size_t i) const {
    return _data[i];
}

template <typename T>
T& Vector<T>::Back() {
    return _data[_size - 1];
}

template <typename T>
const T& Vector<T>::Back() const {
    return _data[_size - 1];
}

template <typename T>
T& Vector<T>::Front() {
    return _data[0];
}

template <typename T>
const T& Vector<T>::Front() const {
    return _data[0];
}

template <typename T>
T* Vector<T>::Data() {
    return _data;
}

template <typename T>
const T* Vector<T>::Data() const {
    return _data;
}

template <typename T>
T* Vector<T>::Realoc(size_t new_capacity) {
    if (new_capacity == 0) {
        return nullptr;
    }
    auto new_data = new uint8_t[sizeof(T) * new_capacity];
    if (_size > 0) {
        std::memcpy(new_data, reinterpret_cast<uint8_t*>(_data), _size * sizeof(T));
    }
    return reinterpret_cast<T*>(new_data);
}

template <typename T>
void Vector<T>::Reserve(size_t new_capacity) {
    if (new_capacity > _capacity) {
        auto new_data = Realoc(new_capacity);
        delete[] reinterpret_cast<uint8_t*>(_data);
        _data = new_data;
        _capacity = new_capacity;
    }
}

template <typename T>
void Vector<T>::Clear() {
    for (size_t i = 0; i < _size; ++i) {
        _data[i].~T();
    }
    _size = 0;
}
template <typename T>
void Vector<T>::PushBack(const T& elem) {
    if (_size == _capacity) {
        size_t new_capacity;
        T* new_data;
        if (_capacity == 0) {
            new_data = Realoc(1);
            new_capacity = 1;
        }
        else {
            new_data = Realoc(_capacity * 2);
            new_capacity = _capacity * 2;
        }
        try {
            new (new_data + _size) T(elem);
        }
        catch (...) {
            delete[] reinterpret_cast<uint8_t*>(new_data);
            throw;
        }
        delete[] reinterpret_cast<uint8_t*>(_data);
        _data = new_data;
        _capacity = new_capacity;
    }
    else {
        new (_data + _size) T(elem);
    }
    ++_size;
}

template <typename T>
void Vector<T>::PushBack(T&& elem) {
    if (_size == _capacity) {
        size_t new_capacity;
        T* new_data;
        if (_capacity == 0) {
            new_data = Realoc(1);
            new_capacity = 1;
        }
        else {
            new_data = Realoc(_capacity * 2);
            new_capacity = _capacity * 2;
        }
        try {
            new (new_data + _size) T(std::move(elem));
        }
        catch (...) {
            delete[] reinterpret_cast<uint8_t*>(new_data);
            throw;
        }
        delete[] reinterpret_cast<uint8_t*>(_data);  //
        _data = new_data;
        _capacity = new_capacity;
    }
    else {
        new (_data + _size) T(std::move(elem));
    }
    ++_size;
}

template <typename T>
void Vector<T>::PopBack() {
    _data[_size - 1].~T();
    --_size;
}

template <typename T>
Vector<T>::Vector(const std::initializer_list<T>& list)
    : _size(list.size())
    , _capacity(_size)
    , _data(_size > 0 ? reinterpret_cast<T*>(new uint8_t[sizeof(T) * _capacity]) : nullptr) {
    size_t i = 0;
    for (const auto& elem : list) {
        new (_data + i) T(elem);
        ++i;
    }
}