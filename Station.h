#pragma once
#include <string>
#include "Vector.h"
#include <iostream>

class Station {                          // ����� ������� (���������������)

private:
	std::string _name;

public:
	
	Station(const std::string& name);

	std::string GetName() const;

	void SetName(const std::string& name);

	virtual Vector<std::string> GetLines() const;

	virtual std::string GetType() const;

	virtual ~Station() = default;
};

class TransferStation:public Station {           // ������� � ��������� 

private:
	Vector<std::string> _lines;

public:
	TransferStation(const std::string& name, const Vector<std::string>& lines);

	Vector<std::string> GetLines() const;

	std::string GetType() const override;

	void AddLine(const std::string& name);
};

struct StationInfo {

	std::string line;
	std::string station;

};

class TransferVertex : public Station {               // ������������ ���� 

private:
	Vector<StationInfo> _stations;

public:
	TransferVertex(const std::string& name, const Vector<StationInfo>& stations);
	Vector<StationInfo> GetStationInfo() const;
	std::string GetType() const override;
    void AddTransfer(const std::string& name, const std::string& line);
	void PrintStationsLines(); 


};

class StationDescriber {                              // ��������� �������

private:
	Station* _station;
	Vector<std::string> _line_name; // ������ �� � ���� ������� ����� ���� �� ���������� ������ 

public:
	StationDescriber(Station* station, const Vector<std::string> line_name);

	~StationDescriber();

	std::string GetStationName() const;

	void SetStationName(const std::string& name);

	void ChangeStationType(const std::string& type);

	std::string GetStationType() const;

	void ShowStation() const; // �������� ������� 
};

class LineDescriber {                             // ��������� ����� 

private:
	Vector<StationDescriber*> _stations;
	std::string _line_name;

public:
	LineDescriber(std::string line_name);

	~LineDescriber();

	LineDescriber(const Vector<StationDescriber*> stations);

	void AddStationDescriber(Station* station, const std::string& line_name); 

	void DeleteStation(const std::string& station_name);

	std::string GetName()const;

	StationDescriber* FindStation(const std::string& line_name); // ���������� ������ ������� � �������

	void ShowTable();                      // �������� ��� ������� ����� ������ ����������       

};


class Metro {
private:
	Vector<LineDescriber> _lines;

public:
	Metro() = default;

	~Metro();

	void ShowMetro();

	void AddLine(const std::string& line_name);

	void AddLine(const std::string& line_name, const std::string& station_name, const std::string& type_name);

	void AddStation(const std::string& line_name,Station* station);

	void DeleteLine(const std::string& line_name);

	void DeleteStation(const std::string& station_name);
	
	void ChangeStationName(const std::string& line_name, const std::string& station_name, const std::string& new_station_name);

	void ChangeStationType(const std::string& line_name, const std::string& station_name, const std::string& type_name);

	void ShowLine(const std::string& line_name);

	StationDescriber* FindStation(const std::string& line_name, const std::string& station_name);

	StationDescriber* FindStation(const std::string& station_name);

	LineDescriber* FindLine(const std::string& line_name);
};
